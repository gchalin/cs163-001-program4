#include "cs_field.h"
/*
	 Hayden Chalin
	 CS163
	 5-20-2024
	 Program 4
	 This file has the member functions for the cs_field class
 */

cs_field::cs_field(){
	// constructor
	title = nullptr;
	definition = nullptr;
	three_aspects = nullptr;
	search_history = nullptr;
	genre = nullptr;
}

cs_field::~cs_field(){
	// desructor
	// XXX The remove function is called in the table destructor
	//remove();
}

int cs_field::create_field(char * temp_key_word, char * temp_def, char * temp_aspects, char * temp_history, char * temp_genre){
	/*
		This function will create an instance of the cs_field
	*/
	title = new char[strlen(temp_key_word) + 1];
	strcpy(title, temp_key_word);

	definition = new char[strlen(temp_def) + 1];
	strcpy(definition, temp_def);

	// change this to create 3 arrays of aspets
	three_aspects = new char[strlen(temp_aspects) + 1];
	strcpy(three_aspects, temp_aspects);

	search_history = new char[strlen(temp_history) + 1];
	strcpy(search_history, temp_history);

	genre = new char[strlen(temp_genre) + 1];
	strcpy(genre, temp_genre);

	return 1;
}

int cs_field::copy(cs_field & field){
	/*
		Copys all current data into a blank cs field
	*/
	if (strlen(field.title)){
		// field is not blank
		field.remove(); // clear it out to fill out
	}
	// copy the title
	field.title = new char[strlen(title)+1];
	strcpy(field.title, title);

	// copy def
	field.definition = new char[strlen(definition)+1];
	strcpy(field.definition, definition);

	// copy three aspects
	field.three_aspects = new char[strlen(three_aspects)+1];
	strcpy(field.three_aspects, three_aspects);

	// copy search history
	field.search_history = new char[strlen(search_history)+1];
	strcpy(field.search_history, search_history);

	// copy genre
	field.genre = new char[strlen(genre)+1];
	strcpy(field.genre, genre);

	return 1; // copy complete
}

int cs_field::display()const{
	// This funciton will display one cs field
	cout << "Key word: " << title << endl;
	cout << "Definition: " << definition << endl;
	cout << "Three aspects: " << three_aspects << endl;
	cout << "Search history: " << search_history << endl;
	cout << "Genre: " << genre << endl;

	return 1;
}


bool cs_field::key_matches(char * key_value){
	// compares title to key passed in

	if (strcmp(title, key_value) == 0)
		return 1;
	else
		return 0;

}

int cs_field::is_less_than(const cs_field compare_val){
//int cs_field::compare(const cs_field compare_val){
/*
	Checks to see if the new value is less than the root value (title)
	returns true if the compared value is less than the root
*/
	cout << endl;
	cout << "*******" << endl;
	cout << "ROOT TITLE: " << title << endl;
	cout << "COMPARE TITLE: " << compare_val.title << endl;
	cout << "STRCMP: " << strcmp(compare_val.title, title) << endl;
	// if compare_val < title
	//if (strcmp(title, compare_val.title) < 0){
	if (strcmp(title, compare_val.title) > 0){
		cout << "Compare val < root" << endl;
			return 1;
		} else{
			cout << "Compare val > root" << endl;
			return 0;
	}
}

int cs_field::remove(){
	// This is the destructor function for each cs_field instance
	if (title){
		delete [] title;
		title = nullptr;
		delete [] definition;
		definition = nullptr;
		delete [] three_aspects;
		three_aspects = nullptr;
		delete [] search_history;
		search_history = nullptr;
		delete [] genre;
		genre = nullptr;
	}
	return 1;

}
