#include "cs_field.h"
#include <iostream>
#include <fstream> // This is used for reading in the file
/*
	 Hayden Chalin
	 CS163
	 5-7-2024
	 This file will contain the class for the tree ADT and member prototypes

 */
/*

	 XXX This is what the data looks like:
	 "Keyword|Description|List of three Aspects|Search History|something else\n"

 */

struct node {
	cs_field field;
	node * right;
	node * left;
};

class tree {

	public:
		tree(void); // make sure to test for 2 different sizes
		~tree(void);
		int insert(const cs_field & to_add);
		int display_all(void) const;
		int remove(char * key_value);
		int retrieve(char * key_value, cs_field & found);
		int get_height(void) const;
		int load(void);

	private:
		node * root; // the root of the tree
		int height; // This will keep track of the height of the tree
		int display_all(int & count, node * root)const;
		int remove(node * & root, char * key_value);
		node * find_IOS(node * root);
		int insert(const cs_field & to_add, node * & root);
		int retrieve(char * key_value, cs_field & found, node * & root);
		int get_height(node * root) const;

		// destructor helper
		void destroy(node * & root);

};
