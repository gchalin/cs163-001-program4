/*
	 Hayden Chalin
	 CS163
	 5-7-2024
	Program 4
	This head file is for the client side of the program.
	It will have the menu choice declarations, and
	prototypes
*/

#include <iostream>
#include <cctype>
#include <cstring>
#include "tree.h"
#include "cs_field.h"

// MENU CHOICES //
const int ADD {1};
const int DISPLAY {2};
const int REMOVE {3};
const int RETRIEVE {4};
const int LOAD_FILE {5};
const int GET_HEIGHT {6};
const int QUIT {9};

using namespace std;

//XXX Grader: These are allowed to be void per Karla's guidelines XXX //
void print_menu(int & menu_choice);
void add_field(tree & BST);
void load_ex_file(tree & BST);
void display_all(tree & BST);
void retrieve(tree & BST);
void remove(tree & BST);
void get_height(tree & BST);
