//#include <fstream> // This is for loading the file into the tree
#include "tree.h"
/*
	 Hayden Chalin
	 CS163
	 5-20-2024
	 Program 4
	 This file has the member functions for the tree class
 */

tree::tree(void){
	// Constructor
	root = nullptr;
}

tree::~tree(void){
	/*
		 Destructor
		 XXX call the cs_field destructor for each node
	 */
	destroy(root); // pvt fn
}

void tree::destroy(node * & root){
	/*
		This is a helper function for the destructor
	*/

	if (!root) return;

	// post order traversal
	destroy(root->left);  destroy(root->right);
	root->field.remove();
	delete root;
	root = nullptr;
	return;
}

int tree::insert(const cs_field & to_add){
// This function adds it to the tree
	if (!root){
		root = new node;
		root->field = to_add;
		root->right = root->left = nullptr;
		return 1;
	}
	// recursive call
	return insert(to_add, root);
}

int tree::insert(const cs_field & to_add, node * & root){
	// This is the recursive function call for adding a CS Field

	if (!root){
		root = new node;
		root->field = to_add;
		root->right = root->left = nullptr;
		return 1;
	}

	// if to_add < root
	if (root->field.is_less_than(to_add)){ // value added is less than root, go left
	 	// XXX Uncomment cout statements to see insertion path
		//cout << "LEFT" << endl;
		insert(to_add, root->left);
	} else {
		//cout << "RIGHT" << endl;
		return insert(to_add, root->right);
	}

	return 1;
}


int tree::remove(char * key_value){
/*
	Remove cs field wrapper function
*/
	if (!root) return 0;

	return remove(root, key_value);

}

int tree::remove(node * & root, char * key_value){
	/*
	This is the recursive call for remove */
	if (!root) return 0; // not found

	// KEY MATCH
	if (root->field.key_matches(key_value)){

		node * hold = root;

		// If key is a leaf node
		if (!root->left && !root->right){
			root->field.remove();
			delete root;
			root = nullptr;
			return 1;
		}

		if (root->left && !root->right){
			root = root->left;
			hold->field.remove();
			delete hold;
			hold = nullptr;
			return 1;
		}

		if (!root->left && root->right){
			root = root->right;
			hold->field.remove();
			delete hold;
			hold = nullptr;
			return 1;
		}

	// now find the ios
		node * IOS = find_IOS(root->right);

		root->field.copy(IOS->field); // Note: this will clear the contents of the IOS
		IOS = nullptr;
		hold = nullptr;
	}

	return remove(root->left, key_value) + remove(root->right, key_value);
}

node * tree::find_IOS(node * root){
	/*
		This function will find the IOS and return it
		Root in the first invocation is root->right
	*/

	if (!root->left){ // only travers left
		return root;
	}

	return find_IOS(root->left);
}

int tree::retrieve(char * key_value, cs_field & found){
	if (!root) return 2;
	return retrieve(key_value, found, root);
};

int tree::retrieve(char * key_value, cs_field & found, node * & root){
	if (!root) return 0;

	if (root->field.key_matches(key_value)){
		//root->field.display();
		root->field.copy(found);
		return 1;
	}

	return retrieve( key_value, found, root->left) + retrieve(key_value, found, root->right);
};

int tree::display_all(void)const{
	/*
		Wrapper function for display all
	*/
	if (!root) return 0;

	int count {0};
	return display_all(count, root);
};

int tree::display_all(int & count, node * root)const{
	/*
		recursive display function
	*/
	if (!root) return 1;
	display_all(count, root->left);
	cout << endl;
	cout << "**********" << endl;
	cout << "Count: " << ++count << endl;
	root->field.display();
	return display_all(count, root->right);
}

int tree::load(void){
/*
	Load the external data file
*/

	const char ex_file_name[50] = "bigdata.txt";
	int count {0};
	ifstream in_file;
	in_file.open(ex_file_name);


	if(in_file){

		// XXX Uncomment this to only load 50, 100 items will not display in the stream XXX//
		//while(in_file && !in_file.eof() && count < 50){
		while(in_file && !in_file.eof()){
			cs_field field;

			// Arrays are size in field.createfield()
			char title[1000];
			char desc[1000];
			char aspects[1000];
			char search_history[1000];
			char genre[1000];

			// Title
			in_file.get(title, 1000, '|');
			in_file.ignore(1000, '|');

			// Description
			in_file.get(desc, 1000, '|');
			in_file.ignore(1000, '|');

			// Aspects
			in_file.get(aspects, 1000, '|');
			in_file.ignore(1000, '|');

			// Search history
			in_file.get(search_history, 1000, '|');
			in_file.ignore(1000, '|');

			// Genre
			in_file.get(genre, 1000, '\n');
			in_file.ignore(1000, '\n');


			// create the cs field XXX This should still work
			field.create_field(title, desc, aspects, search_history, genre);
			insert(field);

			++count;
			cout << endl;
			cout << "COUNT: " << count << endl;
			field.display();
			cout << endl;
		}
		in_file.close();
	}

	cout << count << " Items have been loaded" << endl;
	return count;
};

int tree::get_height(void)const{
	/*
		wrapper function for getting the height of the BST
	*/
	if (!root) return 0;
	return get_height(root);
}

int tree::get_height(node * root)const{
	/*
		Recursive function for getting the height of the BST
	*/
	if (!root) return 0;
	return 1 + max(get_height(root->left),get_height(root->right));
}
